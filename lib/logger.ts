import {createLogger, transports, format} from 'winston'
import appConfig from '../config'

const isProduction = appConfig.env === 'production'

const fileTransport = () =>
  new transports.File({
    filename: appConfig.log_path,
    handleExceptions: true,
  })

const consoleTransport = () => new transports.Console()

const stringifyFormat = format(info => {
  info.PROGRAM = appConfig.app_name
  info.LEVEL_NAME = info.level
  info.MESSAGE = JSON.stringify(info.message)
  delete info.level
  delete info.message

  return info
})

const loggerFormat = format.printf(
  info => `${new Date().toISOString()}: ${info.level} - ${info.message}`,
)

const devFormats = format.combine(
  format.colorize(),
  loggerFormat,
  stringifyFormat(),
)

const prodFormats = format.combine(
  loggerFormat,
  stringifyFormat(),
  format.json(),
)

export const formats = isProduction ? prodFormats : devFormats

const logger = createLogger({
  transports: [],
  format: formats,
})

if (!process.browser) {
  logger.add(fileTransport())
}

if (!isProduction) {
  logger.add(consoleTransport())
}

export default logger
