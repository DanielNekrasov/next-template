import React from 'react'
import cn from 'classnames'
import styles from './Footer.module.scss'

const Footer: React.FC = () => {
  return (
    <footer className={styles.Footer}>
      <div className={cn('container-main', styles.Footer__wrapper)}>
        <a href="https://xsolla.com" target="_blank" rel="noopener noreferrer">
          <img
            src="//cdn.xsolla.net/publisher-v4/logo-xsolla.e37bf7.svg"
            alt=""
          />
        </a>
        <span
          className={styles.Footer__copyright}
        >{`Copyright  © 2006 — ${new Date().getFullYear()} Xsolla Inc.`}</span>
      </div>
    </footer>
  )
}

export default Footer
