import React from 'react'
import Link from 'next/link'
import cn from 'classnames'
import styles from './Navigation.module.scss'
import {FormattedMessage} from 'react-intl'

const Navigation = () => {
  return (
    <nav className={styles.Navigation}>
      <ul className={styles.Navigation__wrap}>
        <li className={styles.Navigation__item}>
          <Link href="/account">
            <a
              className={cn(styles.Navigation__link, {
                [styles['Navigation__link--active']]: true,
              })}
            >
              <FormattedMessage id="app.nav.my-account" />
            </a>
          </Link>
        </li>
        <li className={styles.Navigation__item}>
          <Link href="/events">
            <a className={styles.Navigation__link}>
              <FormattedMessage id="app.nav.all-events" />
            </a>
          </Link>
        </li>
      </ul>
    </nav>
  )
}

export default Navigation
