import React from 'react'
import Navigation from '../Navigation'
import styles from './Header.module.scss'

const Header = () => {
  return (
    <header className={styles.Header}>
      <img
        src="/images/logo-blue.svg"
        className={styles.Header__logo}
        alt="Diamond Club Logo"
        height="72"
      />
      <Navigation />
    </header>
  )
}

export default Header
