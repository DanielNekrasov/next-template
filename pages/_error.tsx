import Error from 'next/error'
import logger from '../lib/logger'

Error.getInitialProps = ({res, err}) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404

  logger.log({
    level: 'error',
    message: `${err.name}: ${err.message}`,
    context: err.stack,
  })

  return {statusCode}
}

export default Error
