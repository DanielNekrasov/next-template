import App, {AppProps, AppContext} from 'next/app'
import {ApolloProvider} from '@apollo/client'
import {IntlProvider} from 'react-intl'
import {ErrorInfo} from 'react'
import RootLayout from '../layouts/RootLayout'
import {APOLLO_STATE_PROP_NAME, initializeApollo} from '../lib/apolloClient'
import '../styles/app.scss'

interface CustomAppProps extends AppProps {
  locale: string
  messages: Record<string, string>
}

class CustomApp extends App<CustomAppProps> {
  componentDidCatch = (error: Error, _errorInfo: ErrorInfo): void => {
    // console.log(error)
    super.componentDidCatch(error, _errorInfo)
  }

  static getInitialProps = async (
    appContext: AppContext,
  ): Promise<CustomAppProps> => {
    const locale = 'en'

    const [messages, appProps] = await Promise.all([
      import('../translations/en.json'),
      App.getInitialProps(appContext),
    ])

    return {
      ...(appProps as any),
      locale,
      messages: messages.default,
    }
  }

  render(): JSX.Element {
    const {Component, pageProps, locale, messages} = this.props
    const state = pageProps[APOLLO_STATE_PROP_NAME]
    const apolloClient = initializeApollo(state)

    return (
      <IntlProvider locale={locale} defaultLocale="en" messages={messages}>
        <ApolloProvider client={apolloClient}>
          <RootLayout>
            <Component {...pageProps} />
          </RootLayout>
        </ApolloProvider>
      </IntlProvider>
    )
  }
}

export default CustomApp
