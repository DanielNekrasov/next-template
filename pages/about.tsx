import React from 'react'
import Link from 'next/link'
import {FormattedMessage} from 'react-intl'
import styles from '../styles/Home.module.scss'

export const About: React.FC = () => {
  return (
    <div className={styles.container}>
      <Link href="/">
        <a>Home</a>
      </Link>
      <p className={styles.description}>
        <FormattedMessage id="app.about" />
      </p>
    </div>
  )
}

export default About
