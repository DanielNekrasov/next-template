import React, {Fragment, useEffect} from 'react'
import cn from 'classnames'
import {FormattedMessage} from 'react-intl'
import config from '../config'
import Footer from '../components/Footer'

import styles from '../styles/page-modules/Promo.module.scss'

export const Home: React.FC = () => {
  useEffect(() => {
    const {Widget} = require('@xsolla/login-sdk')

    const xl = new Widget({
      projectId: config.login_project,
      preferredLocale: 'en_US',
      callbackUrl: 'http://localhost:3000/',
      popupBackgroundColor: '#0073f7',
    })

    xl.mount('widget')
  }, [])

  return (
    <Fragment>
      <div className={styles.Promo}>
        <div className={cn('container-main', styles.Promo__wrapper)}>
          <header className={styles.Promo__header}>
            <img src="/images/logo-white.svg" alt="Diamond club Logo" />
          </header>
          <main className={cn('page-content', styles.Promo__content)}>
            <div className={styles.Promo__col}>
              <h1 className={styles.Promo__title}>
                <FormattedMessage id="app.promo.title" />
              </h1>
              <p>
                <FormattedMessage id="app.promo.paragraph" />
              </p>
            </div>
            <div className={styles.Promo__col}>
              <div id="widget" style={{height: '100%'}}></div>
            </div>
          </main>
        </div>
      </div>
      <Footer />
    </Fragment>
  )
}

export default Home
