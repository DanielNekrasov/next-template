import React from 'react'
import Header from '../components/Header'

const ProfileLayout: React.FC = ({children}) => {
  return (
    <div className="container">
      <Header />
      {children}
    </div>
  )
}

export default ProfileLayout
