import React from 'react'
import Head from 'next/head'

const RootLayout: React.FC = ({children}) => {
  return (
    <div className="page-wrapper">
      <Head>
        <title>Diamond club</title>
        <meta charSet="utf-8" />
        <meta
          name="viewport"
          content="initial-scale=1.0, width=device-width, shrink-to-fit=no"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {children}
    </div>
  )
}

export default RootLayout
