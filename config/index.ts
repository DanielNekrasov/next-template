export default {
  env: process.env.NODE_ENV,
  app_name: 'dc-account',
  log_path: process.env.APP_LOG_PATH || '/',
  login_project: '',
}
